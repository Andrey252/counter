/*
 *  Рыжкин Андрей 14ИТ18к
 */
package com.example.andrey252.counter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    // Начальное значение добавленных студентов
    private Integer countOfStudents = 0;

    /**
     * Вызывается при создании Активности
     * @param savedInstanceState восстановление временных данных при изменении Активности.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * При вызове метода, добавляется студент, счетчик увеличивает значение на 1
     * и сохраняется результат в <code>countOfStudents</code>
     * @param view вьюшка
     */
    public void onClickButtonAddStudents(View view) {
        countOfStudents++;
        outputCountOfStudents();
    }

    /**
     * Метод сохраняет состояния Активности
     * @param outState временные данные в процессе работы Активности
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", countOfStudents);
    }

    /**
     * Метод восстанавливает состояние приложения после изменении Активности
     * @param savedInstanceState данные для восстановления приложения
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null &&
                savedInstanceState.containsKey("count")) {
            countOfStudents = savedInstanceState.getInt("count");
            outputCountOfStudents();
        }
    }

    /**
     * Метод выводит результат о добавленных студентах, передавая в TextView строку
     */
    public void outputCountOfStudents() {
        TextView counterView = (TextView) findViewById(R.id.txt_counter);
        counterView.setText(String.format(Locale.getDefault(), "%d", countOfStudents));
    }
}